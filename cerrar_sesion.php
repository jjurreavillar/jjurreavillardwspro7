<?php
header('Content-type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors','1');
session_start();
session_destroy();
echo 'Se ha cerrado la sesión. Se va a redirigir automáticamente a la página de la que provenía. Si no se le redirige automáticamente, <a href="' . $_SERVER['HTTP_REFERER'] . '">haga clic aquí para volver a la página de la que provenía.</a>';

header('Refresh: 10; url="' . $_SERVER['HTTP_REFERER'] . '"');
?>
