<?php
class Programacion
{
    protected $id;
    protected $nombre;

    public function __construct($id, $nombre)
    {
        $this->id = trim($id);
        $this->nombre = trim($nombre);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
}

class Programador extends Programacion
{
    protected $telefonos;

    public function __construct($id, $nombre, $telefonos)
    {
        parent::__construct($id, $nombre);

        $this->telefonos = $telefonos;
    }

    public function getTelefonos()
    {
        return $this->telefonos;
    }
}

class Software extends Programacion
{
    protected $programador;

    public function __construct($id, $nombre, $programador)
    {
        parent::__construct($id, $nombre);

        $this->programador = trim($programador);
    }

    public function getProgramador()
    {
        return $this->programador;
    }
}

class SQL
{
    private $db;

    public static function Crear($query, $dbName = null)
    {
        $conexion = $dbName == null ? 'mysql:host=localhost' : "mysql:host=localhost;dbname=$dbName";

        try
        {
            $pdo = new PDO($conexion, "alumno", "alumno");
            $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);

            $pdo->query($query);

            $pdo = null;
        }
        catch (PDOException $e)
        {
            header("Error grave");
            echo "<p>ERROR: No se ha podido conectar a la base de datos.</p>";
            echo "<br>";
            echo "<p>ERROR: " . $e->getMessage() . "</p>";
            die();
            exit();
        }
    }

    public function estaConectadaBDD()
    {
        return $this->db != null;
    }

    public function conectarBDD()
    {
        try
        {
            $pdo = new PDO("mysql:host=localhost;dbname=ceedcv", "alumno", "alumno");
            $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);

            $this->db = $pdo;
        }
        catch (PDOException $e)
        {
            $this->desconectarBDD();
        }
    }

    public function consultarBDD($query)
    {
        return $this->db->query($query);
    }

    public function desconectarBDD()
    {
        $this->db = null;
    }
}
?>

