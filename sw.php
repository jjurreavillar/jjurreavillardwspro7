<?php
include_once("config.php");
require("clases.php");

if (USAR_MYSQL == 0)
{
    if (is_file("programadores.txt"))
    {
        $progs = file("programadores.txt");

        foreach($progs as $prog)
        {
            $p = explode(";", $prog);
            $tlf = explode(",", $p[2]);
            $programadores[] = new Programador($p[0], $p[1], $tlf);
        }
    }

    if (is_file("software.txt"))
    {
        $softs = file("software.txt");

        foreach($softs as $soft)
        {
            $s = explode(";", $soft);
            $softwares[] = new Software($s[0], $s[1], $s[2]);
        }
    }
}
else
{
    $sql = new SQL();
    $sql->conectarBDD();

    if ($sql->estaConectadaBDD())
    {
        $query = $sql->consultarBDD("SELECT * FROM programador");

        foreach($query as $campo)
            $programadores[] = new Programador($campo['id'], $campo['nombre'], explode(",", $campo['telefono']));

        $query = $sql->consultarBDD("SELECT * FROM software");

        foreach($query as $campo)
            $softwares[] = new Software($campo['id'], $campo['nombre'], $campo['pId']);   
    }
    else
        echo "<h3>No se ha podido conectar a la base de datos. Asegurese de realizar la instalación.</h3><br><br>";

    $sql->desconectarBDD();
}

foreach($softwares as $soft)
    if ($soft->getId() == $_GET["id"])
    {
        $software = $soft;
        break;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/general.css">
    </head>
    <body>
        <header>
            <h1><?=TITULO?></h1>
        </header>

        <h2>Datos técnicos</h2>
        <div>
            <p>ID interna: <?=$software->getId()?></p>
            <p>Nombre: <?=$software->getNombre()?></p>
            <p>Desarrollador asignado: 
<?php
    foreach($programadores as $programador)
        if ($programador->getId() == $software->getProgramador())
            echo $programador->getNombre();
?></p>
            <br>
        </div>
        <h2>Equipo de desarrollo</h2>
        <div>
<?php
foreach ($programadores as $programador)
    if ($programador->getId() == $software->getProgramador())
    {
        echo "<p>Nombre: ".$programador->getNombre()."</p>";
        echo "<p>Identificador: ".$programador->getId()."</p>";
        foreach ($programador->getTelefonos() as $indice => $tlf)
        {
            echo "<p>";

            if ($indice == 0)
                echo "Teléfonos de contacto: ";

            echo $tlf."</p>";
        }
        echo "<br>";
        }
?>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>

