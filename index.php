<?php
session_start();

include_once("config.php");

if (USAR_MYSQL != 0)
{
    require("clases.php");
    
    $sql = new SQL();
    $sql->conectarBDD();
    
    if ($sql->estaConectadaBDD())
    {
        $consulta = $sql->consultarBDD("SELECT * FROM usuarios");

        foreach($consulta as $valor)
        {
            $nombreUsuarios[] = $valor['nombre'];
            $passUsuarios[] = $valor['pass'];
        }
    }
}
else
{
    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/usuarios.txt")) // En una situación real, este archivo debe almacenarse en otra ubicación, para que ningún usuario pueda visualizarlo
    {
        $contenidos = file($_SERVER['DOCUMENT_ROOT'] . "/usuarios.txt");
        foreach ($contenidos as $indice => $linea)
        {
            $valorLinea = explode("=", $linea, 2);
            
            if ($indice % 2 == 0)
                $nombreUsuarios[] = $valorLinea[1];
            else
                $passUsuarios[] = $valorLinea[1];
        }
    }
    else
    {
        $nombreUsuarios[] = "admin";
        $passUsuarios[] = password_hash("admin", PASSWORD_DEFAULT);
        
        $usuariosArc = fopen($_SERVER['DOCUMENT_ROOT'] . "/usuarios.txt", "wt");
        fwrite($usuariosArc, "nombre=$nombreUsuarios[0]" . PHP_EOL);
        fwrite($usuariosArc, "pass=$passUsuarios[0]" . PHP_EOL);
        fclose($usuariosArc);
    }
}

if (isset($_POST['nombre']) && isset($_POST['pass']) && isset($nombreUsuarios))
{
    $nombreEnviado = htmlspecialchars(trim(strip_tags($_POST['nombre'])));
    $passEnviado = htmlspecialchars(trim(strip_tags($_POST['pass'])));

    $indice = array_search($nombreEnviado, $nombreUsuarios);

    if ($indice !== false && password_verify($passEnviado, $passUsuarios[$indice]))
    {
        $_SESSION["nombre"] = $nombreUsuarios[$indice];
        $_SESSION["autorizado"] = true;

        // Habiendo verificado el inicio de sesión, cambiamos la ID de sesión para reducir el riesgo de ataques por suplantación
        session_regenerate_id();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Proyecto</title>
        <link rel="stylesheet" href="css/general.css">
        <link rel="stylesheet" href="css/principal.css">
    </head>
    <body>
        <header>
<?php 
if (isset($_SESSION["autorizado"]) && $_SESSION["autorizado"])
{
?>
            <a id="cerrarSesion" href="cerrar_sesion.php">Cerrar sesión</a>
<?php
}
?>
            <h1><?=TITULO?></h1>
            <h3>Proyecto UD7 DWC</h3>
        </header>
        <div id="contenedor">
            <article>
<?php
if (isset($_SESSION["autorizado"]) && $_SESSION["autorizado"])
{
?>
                <h2>Elija una opción:</h2>
                <div id="subcontenedor">
                    <div id="programador" class="opcion"><a href="programador.php">Programador</a></div>
                    <div id="software" class="opcion"><a href="software.php">Software</a></div>
                </div>
<?php
}
else
{
?>
                <h2>Inicie sesión</h2>
                <div id="subcontenedor">
                    <form action="index.php" method="POST">
                        <table id="login">
                            <tr>
                                <td>Usuario</td>
                                <td><input type="text" name="nombre"></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td><input type="text" name="pass"></td>
                            </tr>
                            <tr>
                                <td><input type="submit" value="Enviar"></td>
                                <td><input type="reset" value="Borrar"></td>
                            </tr>
                        </table>
                    </form>
<?php
if (isset($_POST['nombre']) && (!isset($_SESSION["autorizado"]) || !$_SESSION["autorizado"]))
{
    echo "<p>Error: Usuario/Password incorrecto</p>";
}
?>
                    <div id="instalacion" class="opcion"><a href="instalacion.php">Instalación</a></div>
                </div>
<?php
}
?>
            </article>
<?php
if (USAR_MYSQL == 0 && is_file("software.txt"))
{
    echo "<aside><nav><p>Software disponible:</p><ul>";
    $listaSw = file("software.txt");
    
    foreach ($listaSw as $sw)
    {
        $campos = explode(";", $sw);

        echo "<li><a href=\"sw.php?id=$campos[0]\" title=\"$campos[1]\">$campos[1]</a></li>";
    }

    echo "</ul></nav></aside>";
}
else if (USAR_MYSQL != 0)
{
    $sql = new SQL();
    $sql->conectarBDD();

    if ($sql->estaConectadaBDD())
    {
        $consulta = $sql->consultarBDD("SELECT COUNT(*) AS cuenta FROM software");

        foreach($consulta as $valor)
            $cuenta = $valor['cuenta'];
    }

    if (isset($cuenta) && $cuenta > 0)
    {
        echo "<aside><nav><p>Software disponible:</p><ul>";

        $consulta = $sql->consultarBDD("SELECT id,nombre FROM software");

        foreach($consulta as $valor)
        {
            echo "<li><a href=\"sw.php?id=$valor[id]\" title=\"$valor[nombre]\">$valor[nombre]</a></li>";
        }

        echo "</ul></nav></aside>";
    }
}
?>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>
